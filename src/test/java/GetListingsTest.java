import Helpers.QueryMaker;
import Helpers.QueryProcessor;
import Pojos.*;
import TestData.Queries;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import static junit.framework.TestCase.assertTrue;

public class GetListingsTest {

    Gson gson;

    QueryMaker maker;

    @Before
    public void before() {
        if (gson != null) {
            gson = null;
        } else if (maker != null) {
            maker = null;
        }
        gson = new Gson();
    }


    @Test
    public void getListing() throws Exception {

        maker = new QueryMaker(Queries.LISTING_EXCHANGEID_TICKER_CURRENCY_ISIN_ISSUEID_LISTINGID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetListing> getListings = feedList.getData().getGetListings();

        QueryProcessor.checkDataInListing(getListings, 20);

    }

    @Test
    public void getPriceSnapshot() throws Exception {

        maker = new QueryMaker(Queries.LISTING_BY_TICKER_CHART_SNAPSHOT_LOW_HIGH_OPEN_CLOSE);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        PriceSnapshot priceSnapshot = feedList.getData().getGetListings().get(0).getChartSnapshot().getPriceSnapshot();
        List<Chart> chartList = feedList.getData().getGetListings().get(0).getChartSnapshot().getCharts();

        Helpers.QueryProcessor.checkDataInPriceSnapshot(chartList, priceSnapshot);

    }
}
