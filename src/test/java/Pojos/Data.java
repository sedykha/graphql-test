package Pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("getRNS")
    @Expose
    private List<GetRN> getRNS = null;

    @SerializedName("getListings")
    @Expose
    private List<GetListing> getListings = null;

    public List<GetRN> getGetRNS() {
        return getRNS;
    }

    public void setGetRNS(List<GetRN> getRNS) {
        this.getRNS = getRNS;
    }

    public List<GetListing> getGetListings() {
        return getListings;
    }

    public void setGetListings(List<GetListing> getListings) {
        this.getListings = getListings;

    }
}
