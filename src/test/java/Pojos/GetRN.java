package Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetRN {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("newsID")
    @Expose
    private String newsID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isins")
    @Expose
    private List<String> isins = null;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("storyId")
    @Expose
    private String storyId;
    @SerializedName("likes")
    @Expose
    private Likes likes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsID() {
        return newsID;
    }

    public void setNewsID(String newsID) {
        this.newsID = newsID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getIsins() {
        return isins;
    }

    public void setIsins(List<String> isins) {
        this.isins = isins;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }



}