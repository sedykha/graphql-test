package Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetListing {

    @SerializedName("ticker")
    @Expose
    private String ticker;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("isin")
    @Expose
    private String isin;
    @SerializedName("issueId")
    @Expose
    private String issueId;
    @SerializedName("listingId")
    @Expose
    private String listingId;
    @SerializedName("chartSnapshot")
    @Expose
    private ChartSnapshot chartSnapshot;

    public ChartSnapshot getChartSnapshot() {
        return chartSnapshot;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public void setChartSnapshot(ChartSnapshot chartSnapshot) {
        this.chartSnapshot = chartSnapshot;
    }

}
