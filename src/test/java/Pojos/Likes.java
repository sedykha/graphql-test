package Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


    public class Likes {

        @SerializedName("count")
        @Expose
        private Count count;

        public Count getCount() {
            return count;
        }

        public void setCount(Count count) {
            this.count = count;
        }

    }

