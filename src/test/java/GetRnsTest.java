import Helpers.QueryMaker;
import Helpers.QueryProcessor;
import Pojos.GetListing;
import Pojos.RNSQuery;
import Pojos.GetRN;
import TestData.Queries;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class GetRnsTest {


    Gson gson;

    QueryMaker maker;

    @Before
    public void before() {
        if (gson != null) {
            gson = null;
        } else if (maker != null) {
            maker = null;
        }
        gson = new Gson();
    }

    @Test
    public void rnsAimPage1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_AIM_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsFtse100Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_FTSE100_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }



    @Test
    public void rnsFtse250Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_FTSE250_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsFtse350Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_FTSE350_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsUk100Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_UK100_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsUk250Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_UK250_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsUk350Page1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_UK350_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsWatchedPage1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_WATCHED_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsAllPage1Limit20Test() throws Exception {

        maker = new QueryMaker(Queries.RNS_ALL_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInRNS(getRNS, 20);

    }


    @Test
    public void rnsIdNewsTitleIsinsTimestampLikes() throws Exception {

        maker = new QueryMaker(Queries.RNS_ID_NEWS_TITLE_ISINS_TIMESTAMP_LIKES_TIMESTAMP_LIKES);
        maker.makeQuery();

        RNSQuery feedList = gson.fromJson(maker.getResponseJson(), RNSQuery.class);

        List<GetRN> getRNS = feedList.getData().getGetRNS();

        QueryProcessor.checkDataInNews(getRNS, 20);

    }

}
