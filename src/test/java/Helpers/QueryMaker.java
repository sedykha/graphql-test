package Helpers;

import TestData.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class QueryMaker {

    private OkHttpClient client;
    private Request request;
    private String json;
    Response response;

    public QueryMaker(){

        this.client = new OkHttpClient();

    }

    public QueryMaker(Enum queryString){

        this.client = new OkHttpClient();
        this.request = new Request.Builder().url(Constants.DEFAULT_ADDRESS.toString() + queryString.toString()).build();

    }

    public QueryMaker(String queryString){

        this.client = new OkHttpClient();
        this.request = new Request.Builder().url(Constants.DEFAULT_ADDRESS.toString() + queryString).build();

    }


    public void makeQuery() throws Exception {

        if(this.request != null){

            try {
                Response response = this.client.newCall(request).execute();

                this.json = response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            throw new Exception("There is no initialized request in the object. Please initialize object by passing queryString to the Constructor or setting it by setter method");
        }

    }

    public String getResponseJson() throws Exception {

        if(this.json.length() > 0){

            return this.json;

        }else{
            throw new Exception("Response json seems to be empty. Did you make query first?");
        }

    }



}
