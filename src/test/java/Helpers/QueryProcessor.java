package Helpers;

import Pojos.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class QueryProcessor {

public static void checkDataInRNS(List<GetRN> list, int desiredSize ){

    assertEquals(list.size(), desiredSize);

    for(int i = 0; i < list.size(); i++){

        System.out.println(list.get(i).getStoryId());
        System.out.println(list.get(i).getTimestamp());
        System.out.println(list.get(i).getTitle());
        assertNotNull(list.get(i).getStoryId());
        assertNotNull(list.get(i).getTitle());
        assertNotNull(list.get(i).getTimestamp());
    }
}

    public static void checkDataInNews(List<GetRN> list, int desiredSize ){

        assertEquals(list.size(), desiredSize);

        for(int i = 0; i < list.size(); i++){

            assertNotNull(list.get(i).getId());
            assertNotNull(list.get(i).getNewsID());
            assertNotNull(list.get(i).getTitle());
            assertNotNull(list.get(i).getIsins());
            assertNotNull(list.get(i).getTimestamp());
            assertNotNull(list.get(i).getLikes().getCount());
            assertNotNull(list.get(i).getLikes().getCount().getDislikes());
            assertNotNull(list.get(i).getLikes().getCount().getLikes());
        }
    }

    public static void checkDataInListing(List<GetListing> list, int desiredSize ){

        for(int i = 0; i < list.size(); i++){

            assertNotNull(list.get(i).getTicker());
            assertNotNull(list.get(i).getCurrencyCode());
            assertNotNull(list.get(i).getIsin());
            assertNotNull(list.get(i).getIssueId());
            assertNotNull(list.get(i).getListingId());
            System.out.println(list.get(i).getListingId());
            System.out.println(list.get(i).getIsin());
            System.out.println(list.get(i).getCurrencyCode());
            System.out.println(list.get(i).getTicker());
        }
    }
    
    public static void checkDataInPriceSnapshot(List<Chart> chartList, PriceSnapshot priceSnapshot){
        Pattern pattern = Pattern.compile("\\d+?.?\\d+");
        for(Chart ch: chartList){
            assertTrue(Helpers.QueryProcessor.matchesPattern(ch.getClose(),pattern));
            assertTrue(Helpers.QueryProcessor.matchesPattern(ch.getHigh(),pattern));
            assertTrue(Helpers.QueryProcessor.matchesPattern(ch.getLow(),pattern));
            assertTrue(Helpers.QueryProcessor.matchesPattern(ch.getOpen(),pattern));
            assertTrue(Helpers.QueryProcessor.isTimestamp(ch.getTimestamp()));
        }

        assertTrue(Helpers.QueryProcessor.matchesPattern(priceSnapshot.getClose(),pattern));
        assertTrue(Helpers.QueryProcessor.matchesPattern(priceSnapshot.getHigh(),pattern));
        assertTrue(Helpers.QueryProcessor.matchesPattern(priceSnapshot.getLow(),pattern));
        assertTrue(Helpers.QueryProcessor.matchesPattern(priceSnapshot.getOpen(),pattern));
        assertTrue(Helpers.QueryProcessor.isTimestamp(priceSnapshot.getTimestamp()));
    }

    public static boolean matchesPattern(String s, Pattern p){
        Matcher matcher = p.matcher(s);
        return matcher.matches();
    }

    public static boolean isTimestamp(String timestamp){

        Date date = new Date(Long.parseLong(timestamp) * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);

        return true;
    }
}
