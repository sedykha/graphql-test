package TestData;

public enum Constants {

    JWT("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjE0MDQ0LCJyb2xlcyI6WyIkYXV0aGVudGljYXRlZCIsIiR3cHVzZXJzIiwiJHdwX2JicF9wYXJ0aWNpcGFudCIsIiRsaXZlUHJpY2luZyIsIndlYmluYXJzIl0sInN0YWxlcyI6MTUxNTUwNzMyNjgxNiwiaWF0IjoxNTE1NTA2NDI2fQ.qoQ1RjjI_zn24mf86sBJ7x-LI8HChuW12vb6Qm6thiw"),

    DEFAULT_ADDRESS("https://api.voxmarkets.co.uk/v2/dev/graphql/graphql?jwt=" + JWT +"&query=");

    private final String text;

    private Constants(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
