package TestData;

public enum Queries {

   RNS_AIM_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: aim, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_FTSE100_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: ftse100, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_FTSE250_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: ftse250, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_FTSE350_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: ftse350, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_UK100_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: uk100, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_UK250_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: uk250, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_UK350_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: uk350, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_WATCHED_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: watched, page: 1, limit: 20){ title timestamp storyId}}"),
   RNS_ALL_PAGE_1_LIMIT_20_TITLE_TIMESTAMP_STORYID("{getRNS(filterIndex: all, page: 1, limit: 20){ title timestamp storyId}}"),
   LISTING_EXCHANGEID_TICKER_CURRENCY_ISIN_ISSUEID_LISTINGID("{getListings(exchangeId:\"5993224227bb7e00119e87c0\"){ ticker,currencyCode, isin, issueId, listingId }}"),
   RNS_ID_NEWS_TITLE_ISINS_TIMESTAMP_LIKES_TIMESTAMP_LIKES("{getRNS(filter: true, page: 1) {id newsID title isins timestamp likes{count {likes dislikes}}}}"),
   LISTING_BY_TICKER_CHART_SNAPSHOT_LOW_HIGH_OPEN_CLOSE("{getListings(tickerShort: \"VOD\") {chartSnapshot{priceSnapshot{low high open close timestamp }charts{ low high open close timestamp }}}}");

    private final String text;

    private Queries(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
